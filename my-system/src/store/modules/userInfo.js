const state = {
  user: {}, //用户
  time: [], //时间
  code: '',
  position: {},
}

const mutations = {
  setUser(state, user) {
    state.user = user
  },
  setTime(state, time) {
    state.time = time
  },
  setCode(state, code) {
    state.code = code
  },
  setPosition(state, positon) {
    state.position = positon
  }
}

const actions = {
  setUser(context, payload) {
    console.log(payload)
    context.commit('setUser', payload)
  }
}

export default {
  state,
  actions,
  mutations
}