import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/store'

Vue.use(VueRouter)

const welcome = () => import('../views/Welcome')
const mineInfo = () => import('../views/MineInfo')
const news = () => import('../views/News')
const userManage = () => import('../views/UserManage')
const rumor = () => import('../views/Rumor')
const mail = () => import('../views/Mail')
const homeUpdatePass = () => import('../views/HomeUpdatePass')
const login = () => import('../views/Login')
const register = () => import('../views/Register')
const home = () => import('../components/Home')
const profile = () => import('../views/Profile')

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  { //注册页面
    path: '/register',
    component: register
  },
  {
    path: '/login',
    component: login
  },
  {
    path: '/home',
    redirect: '/welcome',
    component: home,
    children: [
      { //首页
        path: '/welcome',
        meta: {
          name: '主控制台',
          comp: 'welcome'
        },
        component: welcome
      },
      { //设置个人信息
        path: '/mineInfo',
        meta: {
          name: '设置个人信息',
          comp: 'mineInfo'
        },
        component: mineInfo
      },
      { //谣言粉碎
        path: '/rumor',
        meta: {
          name: '谣言粉碎',
          comp: 'rumor'
        },
        component: rumor
      },
      { //文章分类管理
        path: '/news',
        meta: {
          name: '文章资讯',
          comp: 'news'
        },
        component: news
      },
      {//每日打卡
        path: "/mail",
        meta: {
          name: '每日打卡',
          comp: 'mail'
        },
        component: mail
      },
      { //修改密码
        path: '/updatePass',
        meta: {
          name: '修改密码',
          comp: 'homeUpdatePass'
        },
        component: homeUpdatePass
      },
      {// 数据分析
        path: '/profile',
        meta: {
          name: '数据分析',
          comp: 'profile'
        },
        component: profile
      },
      {// 用户管理
        path: '/userManage',
        meta: {
          name: '用户管理',
          comp: 'userManage'
        },
        component: userManage
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

/*
* 全局前置路由
* */
router.beforeEach((to, from, next) => {
  //校验是否登录，防止不登录，直接进入其他页面
  if ((to.path == '/login' && from.path === '/') || (to.path == '/welcome' && from.path === '/login')) {
    next()
  } else {
    console.log(store.state.userInfo.user)
    if (!store.state.userInfo.user) {
      next('/login')
    } else {
      next()
    }
  }
})


//重写路由的push方法
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error => error)
}

export default router
